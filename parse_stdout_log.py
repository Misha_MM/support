# -*- coding: UTF-8 -*-
# !/usr/bin/env python3
# coding=<utf8>

from connect_bd import connect_to_log_db
from datetime import datetime
from loguru import logger
import psycopg2
import tailer
import time
import sys
import re
import os



backslash_char = '\\'
def main_parse_stdout(path_stdout_log: str, server_name: str, os_name: str) -> None:
    """ Функция для анализа stdout лога сервера. Она открывает лог-файл в режиме чтения в реальном времени (построчно). """

    current_pid: int = os.getpid()
    today_log = datetime.now()
    formatted_date_log = today_log.strftime("%Y-%m-%d-%H-%M-%S")

    logger.remove()
    logger.add(f"log/parse_stdout/{formatted_date_log}.{str(current_pid)}.log",
               mode='a',
               format="{time:YYYY-MM-DD HH:mm:ss.SSS} | {process} | {level} | {file}:{function}:{line} | {message}")
    logger.info("Модуль парсинга stdout логов запущен")

    stdout_file: str = path_stdout_log + "stdout.log"
    cache_id_session: set = set()

    try:
        connect = connect_to_log_db()
        connect.autocommit = True
        cursor = connect.cursor()

        while True:
            try:
                # Открываем лог-файл для чтения
                with open(stdout_file, 'r', encoding='utf-8', errors='ignore') as logfile:
                    if os_name == 'windows' or os_name == 'Windows':
                        logger.info(f"Успешное открытие файла: {stdout_file.split(backslash_char)[-1]}")
                    else:
                        logger.info(f"Успешное открытие файла: {stdout_file.split('/')[-1]}")
                    # Считываем новые строки из лог-файла в реальном времени
                    for row_data in tailer.follow(logfile):
                        # str_data = re.sub(r"\s+$", "", row_data)
                        transceiver_pattern = r"\d{2}/\d{2}/\d{4}\s\d{2}:\d{2}:\d{2}\.\d{3}\s+\|\d+\|\s+.+\|\s+.+\|\s+VS_CircuitProcessControl\((.+)\):\s+started,\s+pid=(\d+)"
                        transceiver_data = re.findall(transceiver_pattern, row_data)

                        if transceiver_data:
                            id_transceiver, pid_transceiver = transceiver_data[0][0], transceiver_data[0][1]
                            logger.info(f"ID трансивера = {id_transceiver}; PID трансивера = {pid_transceiver}")
                            cursor.execute(
                                "INSERT INTO transceiver.id_pid_transceiver (server_name, id, pid) VALUES (%s, %s, %s)",
                                (server_name, id_transceiver, pid_transceiver))

                        session_pattern = r"\d{2}/\d{2}/\d{4}\s\d{2}:\d{2}:\d{2}\.\d{3}\s+\|\d+\|\s+.+\|\s+.+\|\s+ProxiesPool::GetTransceiverProxy Getting transceiver proxy from pool for conference\s=\s'(.+)'"
                        session = re.findall(session_pattern, row_data)

                        if session:
                            if session[0] not in cache_id_session:
                                id_session = session[0]
                                logger.info(f"ID сессии = {id_session}")

                        transceiver_pattern = r"\d{2}/\d{2}/\d{4}\s\d{2}:\d{2}:\d{2}\.\d{3}\s+\|\d+\|\s+.+\|\s+.+\|\s+Found proxy with transceiver name\s=\s'(.+)'"
                        transceiver_data = re.findall(transceiver_pattern, row_data)

                        if transceiver_data:
                            if id_session not in cache_id_session:
                                cache_id_session.add(id_session)
                                id_transceiver = transceiver_data[0]
                                logger.info(f"ID трансивера = {id_transceiver}; ID сессии {id_session}")
                                cursor.execute(
                                    "INSERT INTO transceiver.id_session_id_transceiver (server_name, id_transceiver, id_session) VALUES (%s, %s, %s)",
                                    (server_name, id_transceiver, id_session))

                                cursor.execute("SELECT * FROM transceiver.add_pid_session(_server_name := %s, _id_session := %s)",
                                               (server_name, id_session))

            except FileNotFoundError:
                logger.error(f"Лог-файл {stdout_file} не найден")
                print(f"Лог-файл {stdout_file} не найден")
                time.sleep(5)

    except psycopg2.Error as e:
        logger.error(f"Ошибка при доступе к базе данных LOG: {str(e)}")
        sys.exit(1)

    except Exception as e:
        logger.error(f"Ошибка: {str(e)}")
        sys.exit(1)

    finally:
        cursor.close()
        connect.close()
