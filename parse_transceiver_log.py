# -*- coding: UTF-8 -*-
# !/usr/bin/env python3
# coding=<utf8>


from connect_bd import connect_to_log_db
from datetime import datetime
from loguru import logger
from check_file import *
import configparser
import threading
import psycopg2
import tailer
import json
import sys
import os
import re

dict_users: dict = {}
backslash_char = '\\'

def parse_existing_lines_transceiver(logger, server_name: str, id_session: str, transceiver_file: str, id_conf: str, name_conf: str, os_name: str) -> None:
    """ Функция для анализа трансивер логов. Она единожды открывает полностью лог-файл, анализирует строки, которые
        были записаны в файл перед его открытием. Завершив анализ строк, функция завершает свою работу. """

    if os_name == 'windows' or os_name == 'Windows':
        logger.info(f"Запуск функции parse_existing_lines_transceiver: id_session = {id_session}; id_conf = {id_conf}; "
                    f"name_conf = {name_conf}; file = {transceiver_file.split(backslash_char)[-1]}")
    else:
        logger.info(f"Запуск функции parse_existing_lines_transceiver: id_session = {id_session}; id_conf = {id_conf}; "
                    f"name_conf = {name_conf}; file = {transceiver_file.split('/')[-1]}")

    global dict_users

    try:
        connect = connect_to_log_db()
        connect.autocommit = True
        cursor = connect.cursor()

        # Открываем лог-файл для чтения
        with open(transceiver_file, 'r', encoding='utf-8', errors='ignore') as logfile:
            if os_name == 'windows' or os_name == 'Windows':
                logger.info(f"Успешное открытие файла: {transceiver_file.split(backslash_char)[-1]}")
            else:
                logger.info(f"Успешное открытие файла: {transceiver_file.split('/')[-1]}")
            for row_data in logfile:
                # str_data = re.sub(r"\s+$", "", row_data)
                if fr'Restrict bitrate c={id_session}' in row_data:
                    pattern = rf"\d+/\d+/\d+\s\d+:\d+:\d+.\d+\s\|\d+\|\s+.+\|\s+.+\|\s+Restrict bitrate c={id_session} p=(.+)\[min"
                    user_line = re.findall(pattern, row_data)

                    if user_line:
                        name_user = user_line[0].split('/')[0]  # В name_user входит и ip терминала, пример '#sip:user2@192.168.1.104'
                        id_user = user_line[0].split('/')[1].upper()  # Пример 1BD759936CD45E1726570FDDDEBAF5F4
                        dict_users[f'{id_user}'] = name_user
                        print(dict_users)

                if '): statistics for the last 8 seconds:' in row_data:
                    pattern = rf"\d+/\d+/\d+\s\d+:\d+:\d+.\d+\s\|\d+\|\s+.+\|\s+.+\|\s+RTPSession\((.+)\): statistics for the last 8 seconds:"
                    rtp_statistics = re.findall(pattern, row_data)

                    if rtp_statistics:
                        id_rtp = rtp_statistics[0]

                        if id_rtp in dict_users:  # Проверка на наличие id пользователя в словаре с пользователями текущей сессии
                            rtp_data_dict: dict = {}

                            try:
                                for _ in range(9):
                                    row_data = logfile.readline()
                                    if not row_data:  # Проверка конца файла
                                        break
                                    if 'audio:' not in row_data and 'video:' not in row_data:  # Необходимо, так как порядковое кол-во строк в файле динамическое (1-8 строк)
                                        break

                                    if 'rtp ' in row_data:
                                        key, values = row_data.split(': ')
                                        mf_match = re.search(r'mf="([^"]+)"', values)
                                        bitrate_match = re.search(r'bitrate=(\d+(\.\d)?)', values)
                                        fps_match = re.search(r'fps=(\d+(\.\d)?)', values)

                                        if 'video' in key:
                                            values_dict = {
                                                'mf': mf_match.group(1) if mf_match else None,
                                                'bitrate': bitrate_match.group(1) if bitrate_match else None,
                                                'fps': fps_match.group(1) if fps_match else None
                                            }
                                        else:
                                            values_dict = {
                                                'mf': mf_match.group(1) if mf_match else None,
                                                'bitrate': bitrate_match.group(1) if bitrate_match else None
                                            }

                                        rtp_data_dict[key.strip().replace('  ', ' ').replace(' ', '_')] = values_dict
                                jsonb_rtp_data: str = json.dumps(rtp_data_dict)
                            except:
                                continue

                if '): RTCP statistics' in row_data:
                    pattern = rf"(\d+/\d+/\d+\s\d+:\d+:\d+).\d+\s\|\d+\|\s+.+\|\s+.+\|\s+RTPSession\((.+)\): RTCP"
                    rtcp_statistics = re.findall(pattern, row_data)

                    if rtcp_statistics:
                        date_time, id_rtcp = rtcp_statistics[0][0], rtcp_statistics[0][1]  # Он же id_user, пример 1BD759936CD45E1726570FDDDEBAF5F4

                    if id_rtcp in dict_users:  # Проверка на наличие id пользователя в словаре с пользователями текущей сессии
                        print(f"ID {id_rtcp} есть в словаре")

                        rtcp_data_dict: dict = {}

                        try:
                            for _ in range(8):
                                row_data = logfile.readline()
                                if not row_data:  # Проверка конца файла
                                    break
                                if 'rtp ' not in row_data:  # Необходимо, так как порядковое кол-во строк в файле динамическое (1-8 строк)
                                    break

                                key, values = row_data.split(': ')
                                values_dict = {item.split('=')[0].strip(): item.split('=')[1].strip() for item in values.split(', ')}
                                rtcp_data_dict[key.strip().replace('  ', ' ').replace(' ', '_')] = values_dict

                            # Преобразуем в JSONB
                            jsonb_rtcp_data: str = json.dumps(rtcp_data_dict)
                            cursor.execute(
                                "INSERT INTO transceiver.rtcp_statistics (server_name, id_session, id_conf, name_conf, date_time, id_rtcp, name_user, rtp_data, rtcp_data) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT (server_name, id_session, id_conf, date_time, id_rtcp) DO NOTHING",
                                (server_name, id_session, id_conf, name_conf, date_time, id_rtcp, dict_users[f'{id_rtcp}'], jsonb_rtp_data, jsonb_rtcp_data))

                        except:
                            continue

                    else:
                        print(f"ID {id_rtcp} нету словаре")

    except FileNotFoundError:
        logger.error(f"Лог-файл {transceiver_file} не найден")
        sys.exit(1)

    except psycopg2.Error as e:
        logger.error(f"Ошибка при доступе к базе данных LOG: {str(e)}")
        sys.exit(1)

    except Exception as e:
        logger.error(f"Ошибка: {str(e)}")
        sys.exit(1)

    finally:
        cursor.close()
        connect.close()
        logger.info("Завершение работы функции parse_existing_lines_transceiver")


def parse_new_lines_transceiver(id_session: str, server_name: str, transceiver_file: str, id_conf: str, name_conf: str, os_name: str) -> None:
    """ Функция для анализа трансивер логов. Она открывает лог-файл в режиме чтения в реальном времени (построчно).
        Завершает свою работу, когда конференция завершается и на протяжении 20 секунд файл не обновлялся. """

    if os_name == 'windows' or os_name == 'Windows':
        logger.info(f"Запуск функции parse_new_lines_transceiver: id_session = {id_session}; id_conf = {id_conf}; "
                    f"name_conf = {name_conf}; file = {transceiver_file.split(backslash_char)[-1]}")
    else:
        logger.info(f"Запуск функции parse_new_lines_transceiver: id_session = {id_session}; id_conf = {id_conf}; "
                    f"name_conf = {name_conf}; file = {transceiver_file.split('/')[-1]}")

    global dict_users

    try:
        connect = connect_to_log_db()
        connect.autocommit = True
        cursor = connect.cursor()

        # Открываем лог-файл для чтения
        with open(transceiver_file, 'r', encoding='utf-8', errors='ignore') as logfile:
            if os_name == 'windows' or os_name == 'Windows':
                logger.info(f"Успешное открытие файла: {transceiver_file.split(backslash_char)[-1]}")
            else:
                logger.info(f"Успешное открытие файла: {transceiver_file.split('/')[-1]}")
            # Считываем новые строки из лог-файла в реальном времени
            for row_data in tailer.follow(logfile):
                # str_data = re.sub(r"\s+$", "", row_data)
                if fr'Restrict bitrate c={id_session}' in row_data:
                    pattern = rf"\d+/\d+/\d+\s\d+:\d+:\d+.\d+\s\|\d+\|\s+.+\|\s+.+\|\s+Restrict bitrate c={id_session} p=(.+)\[min"
                    user_line = re.findall(pattern, row_data)

                    if user_line:
                        name_user = user_line[0].split('/')[0]  # В name_user входит и ip терминала, пример '#sip:user2@192.168.1.104'
                        id_user = user_line[0].split('/')[1].upper()  # Пример 1BD759936CD45E1726570FDDDEBAF5F4
                        dict_users[f'{id_user}'] = name_user

                if '): statistics for the last 8 seconds:' in row_data:
                    pattern = rf"\d+/\d+/\d+\s\d+:\d+:\d+.\d+\s\|\d+\|\s+.+\|\s+.+\|\s+RTPSession\((.+)\): statistics for the last 8 seconds:"
                    rtp_statistics = re.findall(pattern, row_data)

                    if rtp_statistics:
                        id_rtp = rtp_statistics[0]

                        if id_rtp in dict_users:  # Проверка на наличие id пользователя в словаре с пользователями текущей сессии
                            rtp_data_dict: dict = {}

                            try:
                                for _ in range(9):
                                    row_data = logfile.readline()
                                    if not row_data:  # Проверка конца файла
                                        break
                                    if 'audio:' not in row_data and 'video:' not in row_data:  # Необходимо, так как порядковое кол-во строк в файле динамическое (1-8 строк)
                                        break

                                    if 'rtp ' in row_data:
                                        key, values = row_data.split(': ')
                                        mf_match = re.search(r'mf="([^"]+)"', values)
                                        bitrate_match = re.search(r'bitrate=(\d+(\.\d)?)', values)
                                        fps_match = re.search(r'fps=(\d+(\.\d)?)', values)

                                        if 'video' in key:
                                            values_dict = {
                                                'mf': mf_match.group(1) if mf_match else None,
                                                'bitrate': bitrate_match.group(1) if bitrate_match else None,
                                                'fps': fps_match.group(1) if fps_match else None
                                            }
                                        else:
                                            values_dict = {
                                                'mf': mf_match.group(1) if mf_match else None,
                                                'bitrate': bitrate_match.group(1) if bitrate_match else None
                                            }

                                        rtp_data_dict[key.strip().replace('  ', ' ').replace(' ', '_')] = values_dict
                                jsonb_rtp_data: str = json.dumps(rtp_data_dict)
                            except:
                                continue

                if '): RTCP statistics' in row_data:
                    pattern = rf"(\d+/\d+/\d+\s\d+:\d+:\d+).\d+\s\|\d+\|\s+.+\|\s+.+\|\s+RTPSession\((.+)\): RTCP"
                    rtcp_statistics = re.findall(pattern, row_data)

                    if rtcp_statistics:
                        date_time, id_rtcp = rtcp_statistics[0][0], rtcp_statistics[0][1]  # Он же id_user, пример 1BD759936CD45E1726570FDDDEBAF5F4

                    if id_rtcp in dict_users:  # Проверка на наличие id пользователя в словаре с пользователями текущей сессии
                        print(f"ID {id_rtcp} есть в словаре")

                        rtcp_data_dict: dict = {}

                        try:
                            for _ in range(8):
                                row_data = logfile.readline()
                                if not row_data:  # Проверка конца файла
                                    break
                                if 'rtp ' not in row_data:  # Необходимо, так как порядковое кол-во строк в файле динамическое (1-8 строк)
                                    break

                                key, values = row_data.split(': ')
                                values_dict = {item.split('=')[0].strip(): item.split('=')[1].strip() for item in values.split(', ')}
                                rtcp_data_dict[key.strip().replace('  ', ' ').replace(' ', '_')] = values_dict

                            # Преобразуем в JSONB
                            jsonb_rtcp_data: str = json.dumps(rtcp_data_dict)
                            cursor.execute(
                                "INSERT INTO transceiver.rtcp_statistics (server_name, id_session, id_conf, name_conf, date_time, id_rtcp, name_user, rtp_data, rtcp_data) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT (server_name, id_session, id_conf, date_time, id_rtcp) DO NOTHING",
                                (server_name, id_session, id_conf, name_conf, date_time, id_rtcp, dict_users[f'{id_rtcp}'], jsonb_rtp_data, jsonb_rtcp_data))
                        except:
                            continue

                    else:
                        print(f"ID {id_rtcp} нету словаре")
                        continue

    except FileNotFoundError:
        logger.error(f"Лог-файл {transceiver_file} не найден")
        sys.exit(1)

    except psycopg2.Error as e:
        logger.error(f"Ошибка при доступе к базе данных LOG: {str(e)}")
        sys.exit(1)

    except Exception as e:
        logger.error(f"Ошибка: {str(e)}")
        sys.exit(1)

    finally:
        cursor.close()
        connect.close()


def main_parse_transceiver(server_name: str, id_session: str, id_conf: str, name_conf: str, os_name: str) -> None:
    """ Функция по подготовке к анализу трансивер логов. Получает и формирует данные перед запуском потоков. """

    current_pid: int = os.getpid()
    today_log = datetime.now()
    formatted_date_log = today_log.strftime("%Y-%m-%d-%H-%M-%S")

    logger.remove()
    logger.add(f"log/parse_transceiver/{formatted_date_log}.{str(current_pid)}.log",
               mode='a',
               format="{time:YYYY-MM-DD HH:mm:ss.SSS} | {process} | {level} | {file}:{function}:{line} | {message}")
    logger.info("Модуль парсинга трансивер логов запущен")

    try:
        config: configparser.ConfigParser = configparser.ConfigParser()
        config.read('config.ini')

        path_transceiver_logs: str = check_path_log(config.get('path_logs', 'path_transceiver_log'), os_name)
        logger.info("Успешное чтение конфиг файла")

    except Exception as e:
        logger.error(f"Ошибка при чтении конфиг файла: {e}")

    today = datetime.now()
    today_date = today.strftime("%Y-%m-%d")

    try:
        with connect_to_log_db() as connect:
            connect.autocommit = True
            with connect.cursor() as cursor:
                cursor.execute("SELECT * FROM transceiver.get_pid_session(_server_name := %s, _id_session := %s)",
                               (server_name, id_session,))
                pid = cursor.fetchall()[0][0]
    except psycopg2.Error as e:
        logger.error(f"Ошибка при доступе к базе данных LOG: {str(e)}")

    transceiver_file: str = ''

    if pid:
        for file in os.listdir(path_transceiver_logs):
            if file.endswith(str(pid) + '.log'):
                current_time = os.path.getmtime(path_transceiver_logs + file)
                current_date = datetime.fromtimestamp(current_time).strftime("%Y-%m-%d")
                if today_date == current_date:
                    if os_name == 'windows' or os_name == 'Windows':
                        transceiver_file: str = path_transceiver_logs + '\\' + file
                    else:
                        transceiver_file: str = path_transceiver_logs + '/' + file
                    logger.info(f"Трансивер лог-файл: {file}, трансивер pid: {pid}")
                    break
    else:
        logger.error("Не удалось получить pid трансивера")
        logger.error("Работа модуля парсинга трансивер логов завершена")
        sys.exit(1)

    if not transceiver_file:
        logger.error('Не удалось найти файл с трансивер логами')
        logger.error("Работа модуля парсинга трансивер логов завершена")
        sys.exit(1)

    else:
        try:
            parce_existing_lines_thread = threading.Thread(target=parse_existing_lines_transceiver,
                                                           args=(logger, server_name, id_session, transceiver_file, id_conf, name_conf, os_name))

            parse_new_lines_thread = threading.Thread(target=parse_new_lines_transceiver,
                                                      args=(id_session, server_name, transceiver_file, id_conf, name_conf, os_name))

            check_mod_transceiver_file_thread = threading.Thread(target=check_mod_file,
                                                                 args=(logger, transceiver_file, current_pid))

            # Запускаем потоки
            parce_existing_lines_thread.start()
            logger.info("Поток parce_existing_lines_thread запущен")
            parse_new_lines_thread.start()
            logger.info("Поток parse_new_lines_thread запущен")
            check_mod_transceiver_file_thread.start()
            logger.info("Поток check_mod_transceiver_file_thread запущен")

            # Ждем завершения выполнения потоков
            parce_existing_lines_thread.join()
            parse_new_lines_thread.join()
            check_mod_transceiver_file_thread.join()

        except Exception as e:
            logger.error(f"Произошла ошибка при запуске потока: {e}")
