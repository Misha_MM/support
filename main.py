# -*- coding: UTF-8 -*-
# !/usr/bin/env python3
# coding=<utf8>


from multiprocessing import freeze_support
from parse_svc_log import main_parse_svc
from parse_stdout_log import main_parse_stdout
from check_tcs_data import get_stats_tcs
from check_file import check_path_log
from clear_log_db import *
from loguru import logger
import configparser
import get_list_sessions
import multiprocessing
import threading
import schedule
import time
import sys

logger.remove()
logger.add(sys.stdout, format="{time:YYYY-MM-DD HH:mm:ss.SSS} | {process} | {level} | {file}:{function}:{line} | {message}")
logger.add("log/stdout.log", mode='a', format="{time:YYYY-MM-DD HH:mm:ss.SSS} | {process} | {level} | {file}:{function}:{line} | {message}")

active_sessions_cache: list[tuple[str, str, str]] = []


def create_processes(active_session: tuple[str, str, str]) -> None:
    """Функция для создания процесса и вызова функции парсинга parse_svc_log.parse_svc"""
    id_session, id_conf, name_conf = active_session
    try:
        create_process = multiprocessing.Process(target=main_parse_svc,
                                                 args=(server_name, id_session, id_conf, name_conf, path_svc_logs, cpu_load, user_bytes, os_name))
        create_process.start()
        logger.info(f'Создан процесс PID: {create_process.pid} для сессии: {id_session}')
    except Exception as e:
        logger.error(f'Ошибка при создании процесса для сессии {id_session}: {str(e)}')


def check_sessions() -> None:
    """ Функция для проверки активных сессий """
    global active_sessions_cache

    schedule.every(2).days.at('02:00').do(lambda: clear_table_active_conferences(logger, server_name))
    schedule.every(2).days.at('02:05').do(lambda: clear_table_transceiver_session(logger, server_name, active_sessions))
    schedule.every(2).days.at('02:10').do(lambda: clear_table_stats_tcs(logger, server_name))
    while True:
        schedule.run_pending()
        try:
            active_sessions: list[tuple[str, str, str]] = get_list_sessions.get_sessions(logger, server_name, active_sessions_cache)
            # Проверяем, изменился ли список активных сессий
            if active_sessions and active_sessions != active_sessions_cache:
                new_sessions = [session for session in active_sessions if session not in active_sessions_cache]
                for session in new_sessions:
                    create_processes(session)
                active_sessions_cache = active_sessions
                logger.info(f"Активные конференции: {active_sessions}")
            else:
                if not active_sessions:
                    # logger.info("Нет активных сессий")
                    active_sessions_cache = active_sessions
                # else:
                #     logger.info("Нет новых активных сессий")
        except Exception as e:
            logger.error(f'Ошибка при проверке активных сессий: {str(e)}')
        time.sleep(5)


if __name__ == '__main__':
    freeze_support()
    logger.info("Парсер запущен")

    config: configparser.ConfigParser = configparser.ConfigParser()

    try:
        config.read('config.ini')
        os_name: str = config.get('other', 'os_name')
        path_svc_logs: str = check_path_log(config.get('path_logs', 'path_svc_logs'), os_name)
        path_stdout_log: str = check_path_log(config.get('path_logs', 'path_stdout_log'), os_name)
        server_name: str = config.get('other', 'server_name')

        cpu_load: int = config.getint('csv', 'cpu_load')
        user_bytes: int = config.getint('csv', 'user_bytes')
        logger.info("Успешное чтение конфиг файла")

        try:
            process = multiprocessing.Process(target=main_parse_stdout,
                                              args=(path_stdout_log, server_name, os_name))
            process.start()
            logger.info(f'Создан процесс PID: {process.pid} для анализа stdout логов сервера')

        except Exception as e:
            logger.error(f'Ошибка при создании процесса по анализу stdout логов сервера: {str(e)}')

    except Exception as e:
        logger.error(f"Ошибка при чтении конфиг файла: {e}")

    try:
        get_list_sessions.write_log_db(logger, server_name, active_sessions_json='[]')

    except Exception as e:
        logger.error(f"Произошла ошибка при записи в LOG БД: {e}")

    try:
        check_sessions_thread = threading.Thread(target=check_sessions)

        check_stats_tcs_thread = threading.Thread(target=get_stats_tcs,
                                                  args=(logger, server_name))

        # Запускаем потоки
        check_sessions_thread.start()
        check_stats_tcs_thread.start()

    except Exception as e:
        logger.error(f"Произошла ошибка при запуске потока: {e}")

    else:
        check_sessions_thread.join()
        check_stats_tcs_thread.join()
