import psycopg2
import configparser


def connect_to_log_db() -> psycopg2.extensions.connection:
    """ Функция для создания подключения к БД логов"""

    # Создание объекта ConfigParser
    config: configparser.ConfigParser = configparser.ConfigParser()
    config.read('config.ini')
    return psycopg2.connect(
        host=config.get('log_server_database', 'host'),
        port=config.getint('log_server_database', 'port'),
        database=config.get('log_server_database', 'database'),
        user=config.get('log_server_database', 'user'),
        password=config.get('log_server_database', 'password')
    )


def connect_to_tcs_db() -> psycopg2.extensions.connection:
    """ Функция для создания подключения к БД tcs"""

    # Создание объекта ConfigParser
    config = configparser.ConfigParser()
    config.read('config.ini')
    return psycopg2.connect(
        host=config.get('tcs_server_database', 'host'),
        port=config.getint('tcs_server_database', 'port'),
        database=config.get('tcs_server_database', 'database'),
        user=config.get('tcs_server_database', 'user'),
        password=config.get('tcs_server_database', 'password')
    )
