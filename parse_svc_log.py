# -*- coding: UTF-8 -*-
# !/usr/bin/env python3
# coding=<utf8>


from parse_transceiver_log import main_parse_transceiver
from connect_bd import connect_to_log_db
from check_file import check_mod_file
from datetime import datetime
from loguru import logger
import multiprocessing
import threading
import psycopg2
import os.path
import tailer
import sys
import re


def parse_svc(logger, server_name: str, id_session: str, id_conf: str, name_conf: str, file: str, cpu_load: int, users_bytes: int, os_name: str) -> None:
    """ Функция для анализа svc логов. Она открывает лог-файл в режиме чтения в реальном времени (построчно).
        Завершает свою работу, когда конференция завершается и на протяжении 20 секунд файл не обновлялся. """

    logger.info(f"Запуск функции parse_svc: id_session = {id_session}; id_conf = {id_conf}; name_conf = {name_conf}; "
                f"file = {'vs_stat_svc_' + id_session + '.txt'}; cpu_load = {cpu_load}; users_bytes = {users_bytes}")
    try:
        connect = connect_to_log_db()
        connect.autocommit = True
        cursor = connect.cursor()
        flag: bool = True

        # Открываем лог-файл для чтения
        with open(file, 'r', encoding='utf-8', errors='ignore') as logfile:
            logger.info(f"Успешное открытие файла: {'vs_stat_svc_' + id_session + '.txt'}")
            # Считываем новые строки из лог-файла в реальном времени
            for row_data in tailer.follow(logfile):
                # str_data = re.sub(r"\s+$", "", row_data)
                if len(row_data) > 15 and row_data.endswith('|') and row_data[-2].isdigit() and row_data[-8] == ',':
                    receiver_pattern = r"(\d{2}/\d{2}/\d{4}\s\d{2}:\d{2}:\d{2})\|\s+(.+)\s\S\s+\d+\,\s+(\d+)\s\|\s+(\d+)\s+\S\s+\d+\,\s+\d+\S\,\s+(\w+)\,\s+\w+\,\s+\d+\,\s+(\d+)\|"
                    receiver_stat = re.findall(receiver_pattern, row_data)

                    if receiver_stat:
                        r_date_time, r_id_user, load, rcv, bndph, bytes_queue = receiver_stat[0][0], receiver_stat[0][1], \
                            receiver_stat[0][2], receiver_stat[0][3], receiver_stat[0][4], receiver_stat[0][5]
                        bytes_queue = int(bytes_queue)
                        load = int(load)
                        print("Receiver:", r_date_time, r_id_user, load, rcv, bndph, bytes_queue)

                        if flag and any(item in r_id_user for item in ['#sip:', '#h323:', '#tel:']):
                            logger.info("В конференции обнаружен sip/h323/tel участник")
                            flag = False
                            create_process = multiprocessing.Process(target=main_parse_transceiver,
                                                                     args=(server_name, id_session, id_conf, name_conf, os_name))
                            create_process.start()
                            logger.info(f'Создан процесс PID: {create_process.pid} для анализа трансивер лога конференции')

                        if bndph != "undef":
                            packets_queue: float = round((bytes_queue * 8 / 1000) / int(bndph), 2)
                        else:
                            packets_queue: float = 0.0

                        if packets_queue > 0.2 and bytes_queue > users_bytes:
                            message = "Проблемы с каналом связи"

                        elif load > cpu_load:
                            message = "Загрузка ЦП"

                        elif load > cpu_load and packets_queue > 0.2 and bytes_queue > users_bytes:
                            message = "Загрузка ЦП; Проблемы с каналом связи"

                        else:
                            message = '_'

                        cursor.execute(
                            "INSERT INTO log_data.receiver_stat (id_session, id_conf, name_conf, date_time, id_user, load, rcv, bndph, bytes_queue, packets_queue, message) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT (id_session, id_conf, date_time, id_user) DO NOTHING",
                            (id_session, id_conf, name_conf, r_date_time, r_id_user, str(load), rcv, bndph, str(bytes_queue), str(packets_queue), message))

                if len(row_data) > 15 and row_data[-1].isdigit() and row_data[-15] == '|':
                    sender_pattern = r"(\d{2}/\d{2}/\d{4}\s\d{2}:\d{2}:\d{2})\|\s+(.+)\s\S\s+(\d+)\s+(\d+)\s+\d+\s+\S+\s+\|\s+\d+\s+\d+\s+\|\s+(\d+)\s+(\d+)"
                    sender_stat = re.findall(sender_pattern, row_data)

                    if sender_stat:
                        s_date_time, s_id_user, b, ba, iv, ia = sender_stat[0][0], sender_stat[0][1], sender_stat[0][2], \
                            sender_stat[0][3], sender_stat[0][4], sender_stat[0][5]
                        print("Sender:", s_date_time, s_id_user, b, ba, iv, ia)

                        if flag and any(item in s_id_user for item in ['#sip:', '#h323:', '#tel:']):
                            logger.info("В конференции обнаружен sip/h323/tel участник")
                            flag = False
                            create_process = multiprocessing.Process(target=main_parse_transceiver,
                                                                     args=(server_name, id_session, id_conf, name_conf, os_name))
                            create_process.start()
                            logger.info(f'Создан процесс PID: {create_process.pid} для анализа трансивер лога конференции')

                        if s_id_user.strip():
                            cursor.execute(
                                "INSERT INTO log_data.sender_stat (id_session, id_conf, name_conf, date_time, id_user, b, ba, iv, ia) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s) ON CONFLICT (id_session, id_conf, date_time, id_user) DO NOTHING",
                                (id_session, id_conf, name_conf, s_date_time, s_id_user, b, ba, iv, ia))

    except FileNotFoundError:
        logger.error(f"Лог-файл {file} не найден")
        sys.exit(1)

    except psycopg2.Error as e:
        logger.error(f"Ошибка при доступе к базе данных LOG: {str(e)}")
        sys.exit(1)

    except Exception as e:
        logger.error(f"Ошибка: {str(e)}")
        sys.exit(1)

    finally:
        cursor.close()
        connect.close()


def main_parse_svc(server_name: str, id_session: str, id_conf: str, name_conf: str, path_svc_logs: str, cpu_load: int, users_bytes: int, os_name: str) -> None:
    """ Функция по подготовке к анализу svc логов. Получает и формирует данные перед запуском потоков. """

    current_pid: int = os.getpid()
    today = datetime.now()
    formatted_date = today.strftime("%Y-%m-%d-%H-%M-%S")

    logger.remove()
    logger.add(f"log/parse_svc/{formatted_date}.{str(current_pid)}.log",
               mode='a',
               format="{time:YYYY-MM-DD HH:mm:ss.SSS} | {process} | {level} | {file}:{function}:{line} | {message}")
    logger.info("Модуль парсинга svc логов запущен")

    file: str = path_svc_logs + "vs_stat_svc_" + id_session + ".txt"
    logger.info(f"SVC лог-файл: {'vs_stat_svc_' + id_session + '.txt'}")

    try:
        # Создаем отдельные потоки для выполнения функций с передачей аргументов
        parse_svc_log_thread = threading.Thread(target=parse_svc,
                                                args=(logger, server_name, id_session, id_conf, name_conf, file, cpu_load, users_bytes, os_name))

        check_mod_svc_file_thread = threading.Thread(target=check_mod_file,
                                                     args=(logger, file, current_pid))

        # Запускаем потоки
        parse_svc_log_thread.start()
        logger.info("Поток parse_svc_log_thread запущен")
        check_mod_svc_file_thread.start()
        logger.info("Поток check_mod_svc_file_thread запущен")

        # Ждем завершения выполнения потоков
        parse_svc_log_thread.join()
        check_mod_svc_file_thread.join()

    except Exception as e:
        logger.error(f"Произошла ошибка при запуске потока: {e}")
