# -*- coding: UTF-8 -*-
# !/usr/bin/env python3
# coding=<utf8>

from connect_bd import *
from typing import Any
import json


def write_log_db(logger, server_name: str, active_sessions_json: str) -> None:
    """ Функция для записи списка текущих активных конференций в log БД. """

    try:
        with connect_to_log_db() as connect:
            connect.autocommit = True
            with connect.cursor() as cursor:
                cursor.execute('INSERT INTO tcs_data.active_conferences (server_name, active) VALUES (%s, %s)',
                               (server_name, active_sessions_json))
    except psycopg2.Error as e:
        logger.error(f"Ошибка при доступе к базе данных LOG: {str(e)}")


def get_sessions(logger, server_name: str, active_sessions_cache: list[tuple[str, str, str]]) -> list[tuple[str, str, str]]:
    """ Функция для получения активных сессий ГРК на сервере tcs и записи полученной информации в log БД. """

    try:
        with connect_to_tcs_db() as connect:
            with connect.cursor() as cursor:
                cursor.execute(f"SELECT id, named_conf_id, topic FROM stat.conferences WHERE end_time is null and type = 5;")
                active_sessions: list[tuple[str, str, str]] | list[tuple[Any, ...]] = cursor.fetchall()  # сохраняем результаты запроса в active_sessions
    except psycopg2.Error as e:
        logger.error(f"Ошибка при доступе к базе данных TCS: {str(e)}")
        active_sessions: list[tuple[str, str, str]] = []

    if active_sessions and active_sessions != active_sessions_cache:
        active_sessions_home_server = [i for i in active_sessions if server_name in i[0]]
        active_sessions_json: str = json.dumps(active_sessions_home_server, ensure_ascii=False)
        write_log_db(logger, server_name, active_sessions_json)
        logger.info(f"Запись в БД активных сессий на TCS сервере: {active_sessions_json}")

    elif not active_sessions and active_sessions != active_sessions_cache:
        active_sessions_json: str = json.dumps([], ensure_ascii=False)
        write_log_db(logger, server_name, active_sessions_json)
        logger.info(f"Запись в БД активных сессий на TCS сервере: {active_sessions_json}")

    return active_sessions
