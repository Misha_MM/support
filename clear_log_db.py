from connect_bd import connect_to_log_db
import psycopg2


def clear_table_active_conferences(logger, server_name: str) -> None:
    """ Функция для удаления записей в таблице log_data.active_conferences.
        Удаляет все записи кроме последней записанной записи в таблицу."""

    try:
        with connect_to_log_db() as connect:
            connect.autocommit = True
            with connect.cursor() as cursor:
                cursor.execute('SELECT tcs_data.delete_active_conferences(_server_name := %s);',
                               (server_name,))
                logger.info(f"Таблица tcs_data.active_conferences очищена")
    except psycopg2.Error as e:
        logger.error(f"Ошибка при доступе к базе данных LOG: {str(e)}")
    finally:
        cursor.close()
        connect.close()


def clear_table_transceiver_session(logger, server_name: str, active_sessions: list[tuple[str, str, str]]) -> None:
    """ Функция для удаления записей в таблице transceiver.transceiver_session.
        Удаляет все записи кроме записей для активных, на момент вызова этой функции, сессий."""

    active_sessions = [id_session[0] for id_session in active_sessions]

    try:
        with connect_to_log_db() as connect:
            connect.autocommit = True
            with connect.cursor() as cursor:
                cursor.execute('SELECT transceiver.delete_transceiver_session(_server_name := %s, _session_list := %s);',
                               (server_name, active_sessions))
                logger.info(f"Таблица transceiver.transceiver_session очищена")
    except psycopg2.Error as e:
        logger.error(f"Ошибка при доступе к базе данных LOG: {str(e)}")
    finally:
        cursor.close()
        connect.close()



def clear_table_stats_tcs(logger, server_name: str) -> None:
    """ Функция для удаления записей в таблице tcs_data.stats_tcs.
        Удаляет все записи кроме последней записанной записи в таблицу."""

    try:
        with connect_to_log_db() as connect:
            connect.autocommit = True
            with connect.cursor() as cursor:
                cursor.execute('SELECT tcs_data.delete_stats_tcs(_server_name := %s);',
                               (server_name,))
                logger.info(f"Таблица tcs_data.stats_tcs очищена")
    except psycopg2.Error as e:
        logger.error(f"Ошибка при доступе к базе данных LOG: {str(e)}")
    finally:
        cursor.close()
        connect.close()
