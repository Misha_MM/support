# -*- coding: UTF-8 -*-
# !/usr/bin/env python3
# coding=<utf8>


import os.path
import signal
import time


def check_path_log(path_log: str, os_name: str) -> str:
    """ Функция для удаления из путей к файлам лишние скобки, если они есть. """

    if (path_log.startswith('"') and path_log.endswith('"')) or (path_log.startswith("'") and path_log.endswith("'")):
        if os_name == 'windows' or os_name == 'Windows':
            path_log = path_log[1:-1] + '\\'
        else:
            path_log = path_log[1:-1] + '/'
        return path_log
    return path_log


def check_mod_file(logger, file: str, current_pid: int) -> None:
    """ Функция для проверки обновления файла. """

    start_time = time.time()
    current_time: float = 0
    while True:
        try:
            current_time = os.path.getmtime(file)
        except FileNotFoundError:
            logger.error(f"Лог-файл {file} не найден")
            os.kill(current_pid, signal.SIGTERM)

        if current_time != start_time:
            print("Файл был изменён!")
            start_time = current_time
        else:
            if time.time() - start_time > 180:
                logger.info("Прошло более 180 секунд без изменений файла. Завершаем работу.")
                os.kill(current_pid, signal.SIGTERM)
        time.sleep(20)
