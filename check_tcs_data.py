# -*- coding: UTF-8 -*-
# !/usr/bin/env python3
# coding=<utf8>

from connect_bd import *
import psycopg2
import time


def write_log_db(logger, server_name: str, stats_tcs: tuple) -> None:
    """ Функция для записи показателей сервера tcs в log БД. """

    if stats_tcs:
        try:
            with connect_to_log_db() as connect:
                connect.autocommit = True
                with connect.cursor() as cursor:
                    cursor.execute(
                        'INSERT INTO tcs_data.stats_tcs (server_name, endpoints, streams, online_users, conferences, participants, guests, gateways, terminals, created_at) '
                        'VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s) '
                        'ON CONFLICT (server_name, created_at) '
                        'DO NOTHING',
                        (server_name, stats_tcs[0], stats_tcs[1], stats_tcs[2], stats_tcs[3], stats_tcs[4], stats_tcs[5], stats_tcs[6],
                         stats_tcs[7], stats_tcs[8].strftime('%Y-%m-%d %H:%M:%S.%f %z')))
        except psycopg2.Error as e:
            logger.error(f"Ошибка при доступе к базе данных LOG: {str(e)}")
    else:
        logger.info("Статистика сервера пустая")


def get_stats_tcs(logger, server_name) -> None:
    """ Функция для получения последней строки в таблице log.stats в tcs БД"""

    while True:
        try:
            with connect_to_tcs_db() as connect:
                with connect.cursor() as cursor:
                    cursor.execute(
                        'SELECT endpoints, streams, online_users, conferences, participants, guests, gateways, terminals, created_at '
                        'FROM log.stats ORDER BY id DESC LIMIT 1;')
                    stats_tcs: tuple = cursor.fetchone()
                    write_log_db(logger, server_name, stats_tcs)
        except psycopg2.Error as e:
            logger.error(f"Ошибка при доступе к базе данных TCS: {str(e)}")
        finally:
            cursor.close()
            connect.close()

        time.sleep(20)
